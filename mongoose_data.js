const express = require('express');
const mongoose = require('mongoose');
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017/New";
mongoose.connect(url);
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser())

let Students = mongoose.model("Students",{
    name:{
        type:'String'
    },
    from: {
        type:'String',
        required:true
    }
});


MongoClient.connect(url, function (err, client) {
  app.post('/mongooseCreateOne', function(req, res1){
        var pro = new Students({
          name:req.body.name,
          from:req.body.from
        })
      pro.save().then(function(){
          console.log("DOne");
        })
      res1.send({
        msg:"Done"
      })
  })
    
    app.post('/mongoosefindOne',function(req,res1){
    var query  = Students.where({ from: req.body.from });
    // console.log(req.body.from);
    query.findOne(function (err, res) {
      if (err) return handleError(err);
      if (res) {
        // doc may be null if no document matched
        // console.log(res);
      }
      res1.send({
        msg:"Done  "+res
      });
    })
    })

  app.post('/mongooseDeleteOne', function (req, res1){
  var query = Students.where ({ from:req.body.from});
  query.deleteOne(function (err, res){
    if (err) return handleError(err);
      if (res){
        msg:'Done'
      }
      res1.send({
        msg:"Done"
      })
  })
  })

  app.post('/mongooseUpdateOne', function (req, res1){
      var query=Students.where({from: req.body.from});
    query.update(query,{from:req.body.fromUpdate}, function (err, res){
      if (err)return handleError(err);
      if (res){
        console.log(res);
        }
    })
    res1.send({
      msg:"doneee"
    })
  })
  
})

app.listen(3000, () => {
  console.log("Running on Port 3000 ");
});
